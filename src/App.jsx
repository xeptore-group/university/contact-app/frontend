import { Col, Layout, Row } from 'antd';
import 'antd/dist/antd.css';
import { Navbar } from 'Components/Navbar';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AddPhoneType } from './Pages/add-phone-type';
import './style.scss';
import { AddContact } from 'Pages/add-contact';
import { Home } from 'Pages/home';
import { ContactDetails } from 'Pages/contact-details';
import { EditContact } from 'Pages/edit-contact';

const { Header, Footer, Content } = Layout;

function App() {
  return (
    <div className="app">
      <Layout>
        <Router>
          <Header>
            <Navbar />
          </Header>
          <Content>
            <Row style={{ paddingTop: '20px' }}>
              <Col span={24}>
                <Row justify="center" type="flex">
                  <Switch>
                    <Route path="/" exact>
                      <Home />
                    </Route>
                    <Route path="/contacts" exact>
                      <Home />
                    </Route>
                    <Route path="/contact/:contactID">
                      <ContactDetails />
                    </Route>
                    <Route path="/edit-contact/:contactID">
                      <EditContact />
                    </Route>
                    <Route path="/add-phone-type" exact>
                      <AddPhoneType />
                    </Route>
                    <Route path="/add-contact" exact>
                      <AddContact />
                    </Route>
                  </Switch>
                </Row>
              </Col>
            </Row>
          </Content>
        </Router>
        <Footer color="light">
          by <a href="https://github.com/xeptore">@xeptore</a>
        </Footer>
      </Layout>
    </div>
  );
}

export default App;

import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Menu, Icon } from 'antd';


const routes = {
  home: '/',
  addPhoneType: '/add-phone-type',
  addContact: '/add-contact',
}

export function Navbar() {
  const location = useLocation();

  return (
    <Menu
      theme="dark"
      mode="horizontal"
      selectedKeys={[location.pathname]}
      style={{ lineHeight: '64px' }}
    >
      <Menu.Item key={routes.home}>
        <Link to={routes.home}>
          <Icon type="home" />
          Home
        </Link>
      </Menu.Item>

      <Menu.Item key={routes.addContact}>
        <Link to={routes.addContact}>
          <Icon type="user-add" />
          Add Contact
          </Link>
      </Menu.Item>
      <Menu.Item key={routes.addPhoneType}>
        <Link to={routes.addPhoneType}>
          <Icon type="phone" />
          Add Phone Type
          </Link>
      </Menu.Item>
    </Menu>
  );
}

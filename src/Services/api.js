import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:5000',
});


export function getContacts() {
  return api.get('/contacts');
}

export function getContact(id) {
  return api.get(`/contacts/${id}`);
}

export function deleteContact(id) {
  return api.delete(`/contacts/${id}`);
}

export function addContact(form, file) {
  const formData = new FormData();
  formData.append('FirstName', form.FirstName);
  formData.append('LastName', form.LastName);
  formData.append('Type', form.Type);
  if (form.Phones) {
    form.Phones.forEach((phone, index) => {
      formData.append(`Phones[${index}].Number`, phone.Number);
      formData.append(`Phones[${index}].TypeID`, phone.TypeID);
    });
  }
  formData.append('Pics', file);
  return api.post('/contacts',
    formData,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
  );
}

export function updateContact(contactID, form, file) {
  const formData = new FormData();
  formData.append('FirstName', form.FirstName);
  formData.append('LastName', form.LastName);
  formData.append('Type', form.Type);
  if (form.Phones) {
    form.Phones.forEach((phone, index) => {
      formData.append(`Phones[${index}].Number`, phone.Number);
      formData.append(`Phones[${index}].TypeID`, phone.TypeID);
    });
  }
  formData.append('Pics', file);
  return api.put(`/contacts/${contactID}`,
    formData,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
  );
}

export function getPhoneTypes() {
  return api.get('/contact-phone-types');
}

export function addPhoneType(name) {
  return api.post('/contact-phone-types', { Name: name });
}

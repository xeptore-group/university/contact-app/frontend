import React, { useState, useEffect } from 'react';
import { last } from 'lodash';
import { Alert, Typography, Col } from 'antd';
import { getPhoneTypes, updateContact, getContact } from 'Services/api';
import Form from './Form';
import { useParams } from 'react-router-dom';


const { Title } = Typography;

export default function EditContact() {
  const { contactID } = useParams();
  const [contact, setContact] = useState(null);
  const [getContactError, setGetContactError] = useState(false);
  const [submitSuccess, setSubmitSuccess] = useState(false);
  const [submitError, setSubmitError] = useState(false);
  const [getPhoneTypesError, setGetPhoneTypesError] = useState(false);
  const [phoneTypes, setPhoneTyes] = useState([]);

  useEffect(() => {
    getPhoneTypes()
      .then((res) => res.data)
      .then((data) => {
        setPhoneTyes(data);
      })
      .catch(() => {
        setGetPhoneTypesError(true);
      });

    getContact(contactID)
      .then((res) => res.data)
      .then((data) => {
        data.imageSrc = `http://localhost:5000/${last(data.images).path}`;
        setContact(data);
      })
      .catch(() => {
        setGetContactError(true);
      });
  }, [contactID]);

  const handleSubmit = (fields, file) => {
    updateContact(contactID, fields, file)
      .then((res) => {
        setSubmitSuccess(true);
      })
      .catch((e) => {
        setSubmitError(true);
      });
  }


  return (
    <>
      <Col span={24}>
        <Title level={2} align="center">Edit Contact</Title>
      </Col>
      <Col xs={{ span: 20 }} md={{ span: 8 }}>
        {
          getPhoneTypesError ? (<Alert closable message="Error getting phone types." type="error" />) : (null)
        }
        {
          getContactError ? (<Alert closable message="Error getting contact details." type="error" />) : (null)
        }
        {
          !getPhoneTypesError && phoneTypes.length > 0 && !getContactError && contact !== null
            ? (<Form contact={contact} onSubmit={handleSubmit} phoneTypes={phoneTypes} />)
            : (null)
        }
        {
          submitSuccess ? (<Alert closable message="Contact successfully added." type="success" />) : (null)
        }
        {
          submitError ? (<Alert closable message="Error registering contact." type="error" />) : (null)
        }
      </Col>
    </>
  )
}

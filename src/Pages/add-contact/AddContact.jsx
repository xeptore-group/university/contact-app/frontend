import React, { useState, useEffect } from 'react';
import { Alert, Typography, Col } from 'antd';
import { getPhoneTypes, addContact } from 'Services/api';
import Form from './Form';


const { Title } = Typography;

export default function AddContact() {
  const [submitSuccess, setSubmitSuccess] = useState(false);
  const [submitError, setSubmitError] = useState(false);
  const [getPhoneTypesError, setGetPhoneTypesError] = useState(false);
  const [phoneTypes, setPhoneTyes] = useState([]);

  useEffect(() => {
    getPhoneTypes()
      .then((res) => res.data)
      .then((data) => {
        setPhoneTyes(data);
      })
      .catch(() => {
        setGetPhoneTypesError(true);
      });
  }, []);

  const handleSubmit = (fields, file) => {
    addContact(fields, file)
      .then((res) => {
        setSubmitSuccess(true);
      })
      .catch((e) => {
        setSubmitError(true);
      });
  }


  return (
    <>
      <Col span={24}>
        <Title level={2} align="center">New Contact</Title>
      </Col>
      <Col xs={{ span: 20 }} md={{ span: 8 }}>
        {
          getPhoneTypesError ? (<Alert closable message="Error getting phone types." type="error" />) : (null)
        }
        {
          !getPhoneTypesError && phoneTypes.length > 0
            ? (<Form onSubmit={handleSubmit} phoneTypes={phoneTypes} />)
            : (null)
        }
        {
          submitSuccess ? (<Alert closable message="Contact successfully added." type="success" />) : (null)
        }
        {
          submitError ? (<Alert closable message="Error registering contact." type="error" />) : (null)
        }
      </Col>
    </>
  )
}

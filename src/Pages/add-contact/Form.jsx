import React, { Component } from 'react';
import { Radio, Form, Icon, Input, Button, Select } from 'antd';
import './style.scss';


const { Option } = Select;

function hasErrors(fieldsError, phonesError) {
  const fieldsHaveAnyError = Object.keys(fieldsError).some(field => fieldsError[field]);
  if (phonesError) {
    const phonesHaveError = phonesError
      .map(pe => pe.number)
      .some(number => number);
    return (fieldsHaveAnyError || phonesHaveError);
  }
  return fieldsHaveAnyError;
}

let id = 0;

class AddContactForm extends Component {
  constructor() {
    super();

    this.state = {
      featuredImage: null,
    };
  }


  componentDidMount() {
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const formData = {
          FirstName: values.firstName,
          LastName: values.lastName,
          Type: values.type,
        };
        if (values.phones) {
          formData.Phones = values.phones.map((p) => ({
            Number: p.number,
            TypeID: p.type,
          }));
        }
        this.props.onSubmit(formData, this.state.featuredImage);
      }
    });
  };

  validatePhoneNumber = (rule, value, cb) => {
    if (value && !/^[1-9][0-9]{9}$/.test(value)) {
      return cb('Invalid phone number. It must be in 10 character long number. e.g. 9123456789.');
    }
    return cb();
  }

  handleFeatureImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      const { files } = e.target;
      this.setState({
        featuredImage: files[0],
      });
    }
  }

  remove = (k) => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 1) {
      return;
    }

    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    }, () => {
      form.validateFields();
    });
  }

  add = () => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);

    form.setFieldsValue({
      keys: nextKeys,
    }, () => {
      form.validateFields();
    });
  }

  render() {
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched,
      getFieldValue } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 24 },
      wrapperCol: { span: 24 },
    };

    const formItemWithOutLabel = {
      wrapperCol: { span: 24 },
    }

    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const phoneItems = keys.map((k, index) => {
      const fieldName = `phones[${k}].number`;
      const fieldError = isFieldTouched(fieldName) && getFieldError(fieldName);

      return (
        <Form.Item
          {...(index === 0 ? formItemLayout : formItemWithOutLabel)}
          validateStatus={fieldError ? 'error' : ''}
          help={fieldError || ''}
          label={index === 0 ? 'Phone Numbers:' : ''}
          required={false}
          key={k}
        >
          {getFieldDecorator(fieldName, {
            validateTrigger: ['onBlur', 'onChange'],
            rules: [
              {
                required: true,
                whitespace: true,
                message: 'Please enter a valid phone number or delete this field.',
              },
              {
                validator: this.validatePhoneNumber,
              }
            ],
          })(
            <Input style={{ width: keys.length > 1 ? '60%' : '65%' }} placeholder="9123456789" />
          )}
          {getFieldDecorator(`phones[${k}].type`, {
            initialValue: this.props.phoneTypes[0].id,
          })(
            <Select style={{ width: keys.length > 1 ? '30%' : '35%' }}>
              {
                this.props.phoneTypes.map((pt) => (
                  <Option key={pt.id} value={pt.id}>{pt.name}</Option>
                ))
              }
            </Select>
          )}
          {
            keys.length > 1 ? (
              <Icon
                style={{ width: '10%' }}
                className="dynamic-delete-button"
                type="minus-circle-o"
                onClick={() => this.remove(k)}
              />
            ) : (null)
          }
        </Form.Item>
      )
    });

    // Only show error after a field is touched.
    const firstNameError = isFieldTouched('firstName') && getFieldError('firstName');
    const lastNameError = isFieldTouched('lastName') && getFieldError('lastName');
    const typeError = isFieldTouched('type') && getFieldError('type');

    return (
      <Form layout="vertical" onSubmit={this.handleSubmit}>
        <Form.Item label="First Name" validateStatus={firstNameError ? 'error' : ''} help={firstNameError || ''}>
          {getFieldDecorator('firstName', {
            validateTrigger: ['onChange', 'onBlur'],
            validateFirst: true,
            rules: [
              {
                required: true,
                whitespace: true,
                message: 'Please input first name!',
              },
            ],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Ali"
            />,
          )}
        </Form.Item>

        <Form.Item label="Last Name" validateStatus={lastNameError ? 'error' : ''} help={lastNameError || ''}>
          {getFieldDecorator('lastName', {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                required: true,
                whitespace: true,
                message: 'Please input last name!',
              },
            ],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Alavi"
            />
          )}
        </Form.Item>

        <Form.Item
          label="Real/Legal Contact Type"
          validateStatus={typeError ? 'error' : ''}
          help={typeError || ''}
          style={{ textAlign: 'center' }}
        >
          {getFieldDecorator('type', {
            rules: [{ required: true, message: 'Please select a valid person type!' }],
            initialValue: 'Real',
          })(
            <Radio.Group style={{ width: '100%', textAlign: 'center' }} buttonStyle="solid">
              <Radio.Button style={{ width: '50%' }} value="Real">Real</Radio.Button>
              <Radio.Button style={{ width: '50%' }} value="Legal">Legal</Radio.Button>
            </Radio.Group>
          )}
        </Form.Item>

        <Form.Item required label="Image">
          <Input
            id="imageInput"
            name="file"
            className="custom-file-input"
            onChange={e => this.handleFeatureImageChange(e)}
            type="file"
            accept=".png,.jpg,.jpeg"
          />
          <label htmlFor="imageInput">
            <span style={{ fontSize: 'larger' }}>
              {
                this.state.featuredImage === null
                  ? (<Icon type="loading" />)
                  : (<Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />)
              }
            </span> <span>Choose An Image</span>
          </label>
        </Form.Item>

        {phoneItems}

        <Form.Item>
          <Button type="dashed" onClick={this.add} block>
            <Icon type="plus" /> Add Phone
          </Button>
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            block
            disabled={
              hasErrors(
                getFieldsError(['firstName', 'lastName', 'type']),
                getFieldError('phones')
              ) || this.state.featuredImage === null
            }
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}


export default Form.create({ name: 'ad_contact_form' })(AddContactForm);

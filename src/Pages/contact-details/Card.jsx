import React, { useState } from 'react';
import { List, Badge, Card } from 'antd';

const { Meta } = Card;


export default function ContactCard({ contact }) {
  const [key, setKey] = useState('info');

  const tabList = [
    {
      key: 'info',
      tab: 'Info',
    },
    {
      key: 'phones',
      tab: (
        <Badge showZero count={contact.phones.length} offset={[5, -5]}>
          <p>Phone Numbers</p>
        </Badge>
      ),
    },
  ];

  const contentList = {
    phones: (
      <div>
        <List
          bordered
          dataSource={contact.phones}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                title={item.number}
                description={item.type.name}
              />
            </List.Item>
          )}
        />

      </div>
    )
    ,
    info: (
      <div>
        <Meta
          title={`${contact.firstName} ${contact.lastName}`}
          description={
            <div>
              <p>
                Added At: <strong>{contact.dateAdded.toLocaleDateString() + " " + contact.dateAdded.toLocaleTimeString()}</strong>
              </p>
              <p>
                Type: <strong>{contact.type}</strong>
              </p>
            </div>
          }
        />
      </div>
    ),
  };

  return (
    <Card
      hoverable
      style={{ width: '70%', margin: '0 auto' }}
      tabList={tabList}
      activeTabKey={key}
      onTabChange={key => setKey(key)}
      cover={
        key === 'info'
          ? (<img alt="example" src={contact.imageSrc} />)
          : (null)
      }
    >
      {contentList[key]}
    </Card>
  )
}

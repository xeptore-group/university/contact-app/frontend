import React, { useState, useEffect } from 'react';
import { last } from 'lodash';
import { useParams } from 'react-router-dom';
import { Alert, Col, Typography } from 'antd';
import { getContact } from 'Services/api';
import Card from './Card';


const { Title } = Typography;

export default function ContactDetails() {
  const { contactID } = useParams();
  const [getError, setGetError] = useState(false);

  const [contact, setContact] = useState(null);

  useEffect(() => {
    getContact(contactID)
      .then((res) => {
        console.log('res:', res.data);
        const contact = res.data;
        contact.dateAdded = new Date(contact.dateAdded);
        if (contact.images.length > 0) {
          contact.imageSrc = `http://localhost:5000/${last(contact.images).path}`;
        }
        setContact(contact);
      })
      .catch(() => {
        setGetError(true);
      });
  }, [contactID]);

  return (
    <>
      <Col span={24}>
        <Title level={2} align="center">Contact Details</Title>
      </Col>
      <Col xs={24} sm={16} md={8}>
        {
          getError
            ? (<Alert closable message="An error occurred receving contact details." type="error" />)
            : (null)
        }
        {
          contact
            ? (<Card contact={contact} />)
            : (null)
        }
      </Col>
    </>
  )
}
import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';


function hasErrors(fieldsError, phonesError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class AddPhoneTypeForm extends Component {
  componentDidMount() {
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values);
      }
    });
  };

  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const nameError = isFieldTouched('name') && getFieldError('name');

    return (
      <Form layout="vertical" onSubmit={this.handleSubmit}>
        <Form.Item label="Type Name" validateStatus={nameError ? 'error' : ''} help={nameError || ''}>
          {getFieldDecorator('name', {
            validateTrigger: ['onChange', 'onBlur'],
            validateFirst: true,
            rules: [
              {
                required: true,
                whitespace: true,
                message: 'Please input a type name!',
              },
            ],
          })(
            <Input placeholder="Irancell" />,
          )}
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            block
            disabled={hasErrors(getFieldsError())}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}


export default Form.create({ name: 'add_phone_type_form' })(AddPhoneTypeForm);

import React, { useState } from 'react';
import { Alert, Typography, Col } from 'antd';
import { addPhoneType } from 'Services/api';
import Form from './Form';


const { Title } = Typography;

export default function AddPhoneType() {
  const [submitSuccess, setSubmitSuccess] = useState(false);
  const [submitError, setSubmitError] = useState(false);

  const handleFormSubmit = (values) => {
    addPhoneType(values.name)
      .then(() => {
        setSubmitSuccess(true);
      })
      .catch(() => {
        setSubmitError(true);
      });
  }


  return (
    <>
      <Col span={24}>
        <Title level={2} align="center">New Phone Type</Title>
      </Col>
      <Col span={8}>
        <Form onSubmit={handleFormSubmit} />
        {
          submitSuccess ? (<Alert closable message="Phone type successfully added." type="success" />) : (null)
        }
        {
          submitError ? (<Alert closable message="Error registering phone type." type="error" />) : (null)
        }
      </Col>
    </>
  )
}

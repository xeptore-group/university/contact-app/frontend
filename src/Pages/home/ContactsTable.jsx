import React from 'react';
import { Link } from 'react-router-dom';
import { last } from 'lodash';
import { Avatar, Icon, Tooltip, Button, Table, Divider, Tag } from 'antd';
import './style.scss';


const columns = (onDeleteClick) => ([
  {
    title: 'Image',
    dataIndex: 'image',
    key: 'image',
    className: 'image-col',
    align: 'center',
    width: 10,
    render: (text, record) => <Avatar size="large" src={
      record.images.length > 0
        ? `http://localhost:5000/${last(record.images).path}`
        : ''
    } />,
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: (text, record) => <Link to={`/contact/${record.id}`}>{text}</Link>,
  },
  {
    title: 'Type',
    key: 'type',
    dataIndex: 'type',
    render: type => (
      <span>
        {
          type === 'Real' ?
            (
              <Tag color="green">
                {type.toUpperCase()}
              </Tag>
            ) :
            (
              <Tag color="pink">
                {type.toUpperCase()}
              </Tag>
            )
        }
      </span>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
        <Tooltip title={`Edit: ${record.name}`} placement="top">
          <Link to={`/edit-contact/${record.id}`}>
            <Icon type="edit" />
          </Link>
        </Tooltip>

        <Divider type="vertical" />

        <Tooltip title={`Delete: ${record.name}`} placement="top">
          <Button type="danger" icon="delete" onClick={() => onDeleteClick(record)} />
        </Tooltip>
      </span>
    ),
  },
]);

export function ContactsTable({ contacts, onDelete }) {
  const showContactDeleteModal = (contact) => {
    onDelete(contact);
  }

  return (
    <>
      <Table
        rowKey="id"
        columns={columns(showContactDeleteModal)}
        dataSource={contacts} />
    </>
  );
}

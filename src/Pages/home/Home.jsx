import React, { useEffect, useState } from 'react';
import { Modal, Alert, Typography, Col } from 'antd';
import { deleteContact, getContacts } from 'Services/api';
import { ContactsTable } from './ContactsTable';


const { Title } = Typography;

export default function Home() {
  const [err, setErr] = useState(false);
  const [contacts, setContacts] = useState([]);
  const [modalIsDeleting, setModalIsDeleting] = useState(false);
  const [mustFetch, setMustFetch] = useState(false);

  useEffect(() => {
    getContacts()
      .then((res) => res.data)
      .then((data) => {
        setContacts(
          data.map((c => ({
            ...c,
            name: `${c.firstName} ${c.lastName}`,
          })))
        );
      })
      .catch(() => {
        setErr(true);
      })
  }, [mustFetch]);

  const handleContactDelete = (contact) => {
    Modal.confirm({
      title: 'Deleting Contact',
      onOk() {
        setModalIsDeleting(true);
        return deleteContact(contact.id)
          .then(() => getContacts())
          .then((res) => {
            setContacts(res.data);
            setModalIsDeleting(false);
            setMustFetch(x => !x);
          });
      },
      okType: 'danger',
      okText: 'Delete',
      closable: false,
      cancelButtonProps: { disabled: modalIsDeleting },
      content: (
        <>
          <p>
            Are you sure you want to remove contact?
          </p>
          <p>
            <em>{contact.name}</em>
          </p>
        </>
      )
    });
  }

  return (
    <>
      <Col span={24}>
        <Title level={2} align="center">Contacts List</Title>
      </Col>
      <Col span={14}>
        {
          err
            ? (<Alert closable message="An error occurred fetching contacts list." type="error" />)
            : (<ContactsTable contacts={contacts} onDelete={handleContactDelete} />)
        }

      </Col>
    </>
  );
}
